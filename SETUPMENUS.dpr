program SETUPMENUS;

uses
  Vcl.Forms,
  SetupU in 'SetupU.pas' {Setupfrm},
  Vcl.Themes,
  Vcl.Styles,
  CloneUserU in 'CloneUserU.pas' {CloneUser};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Amethyst Kamri');
  Application.Title := 'Citrix Application Menu Setup';
  Application.CreateForm(TSetupfrm, Setupfrm);
  Application.Run;
end.
