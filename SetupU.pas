{ -----------------------------------------------------------------------------
  Unit Name: SetupU
  Author:    StephenHughes
  Date:      07-Nov-2013
  Purpose:
  History: v1.0
  This unit allows user to setup menu options for all logon users, to add new users
  and to add new menus.
  the menu must have a fully qualified file name   The menu id must be unique but
  this is not inforced Oracle does not have an autoinc number without setting
  up a trigger
  ----------------------------------------------------------------------------- }

Unit SetupU;

Interface

Uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Data.DB, Vcl.Grids, Vcl.DBGrids, OracleData,
  Oracle, OracleNavigator,
  RxLookup, Vcl.StdCtrls, Vcl.Buttons, Vcl.ComCtrls, Vcl.ImgList;

Type
  TSetupfrm = Class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Session: TOracleSession;
    Logon: TOracleLogon;
    Menudata: TOracleDataSet;
    Menudatads: TDataSource;
    Usersds: TDataSource;
    DBGrid1: TDBGrid;
    MenudataTITLE: TStringField;
    MenudataCOMMANDLINE: TStringField;
    MenudataPARAMATER: TStringField;
    MenudataICON_NUMBER: TFloatField;
    MenudataID: TFloatField;
    OracleNavigator1: TOracleNavigator;
    DBGrid2: TDBGrid;
    OracleNavigator2: TOracleNavigator;
    Users: TOracleDataSet;
    UsersUSER_ID: TStringField;
    UsersFIRST_NAME: TStringField;
    UsersLAST_NAME: TStringField;
    UsersLOGONNAME: TStringField;
    UsersUSER_NO: TFloatField;
    UserMenuDS: TDataSource;
    Usermenus: TOracleDataSet;
    Q1: TOracleQuery;
    UsersMenus: TOracleDataSet;
    UsersMenusDS: TDataSource;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    CbUser: TComboBoxEx;
    Label2: TLabel;
    Cbmenu: TComboBoxEx;
    BtnUpdate: TBitBtn;
    GroupBox2: TGroupBox;
    DBGrid3: TDBGrid;
    OracleNavigator3: TOracleNavigator;
    LargeImages: TImageList;
    SpeedButton1: TSpeedButton;
    BitBtn1: TBitBtn;
    BtnDeleteMenuOption: TBitBtn;
    BalloonHint1: TBalloonHint;
    EdFind: TLabeledEdit;
    SpeedButton2: TSpeedButton;
    MenudataGROUP_NAME: TStringField;
    MenudataAPP_DESCRIPTION: TStringField;
    MenudataAPP_IMAGE: TBlobField;
    BitBtn2: TBitBtn;
    UsersMenusTITLE: TStringField;
    UsersMenusLOGONNAME: TStringField;
    UsersMenusID: TFloatField;
    Procedure FormShow(Sender: TObject);
    Procedure BtnUpdateClick(Sender: TObject);
    Procedure DBGrid1DrawColumnCell(Sender: TObject; Const Rect: TRect; DataCol: Integer;
      Column: TColumn; State: TGridDrawState);
    Procedure SpeedButton1Click(Sender: TObject);
    Procedure OracleNavigator1Click(Sender: TObject; Button: TOracleNavigateBtn);
    Procedure BtnDeleteMenuOptionClick(Sender: TObject);
    Procedure EdFindKeyPress(Sender: TObject; Var Key: Char);
    Procedure SpeedButton2Click(Sender: TObject);
    Procedure DBGrid3CellClick(Column: TColumn);
    Procedure DBGrid1TitleClick(Column: TColumn);
    Procedure DBGrid2TitleClick(Column: TColumn);
    Procedure FormCreate(Sender: TObject);
    Procedure BitBtn2Click(Sender: TObject);
  Private
    Procedure UpdateGUIControls;
    { Private declarations }
  Public
    { Public declarations }
  End;

Var
  Setupfrm: TSetupfrm;

Implementation

{$R *.dfm}

uses CloneUserU;

Procedure TSetupfrm.BitBtn2Click(Sender: TObject);
var
  I: Integer;
Begin
  If CbUser.Text = '' Then
    Exit
  Else
  Begin
    If MessageDlg('Do you want to copy  the settings of this user '+cbuser.Text+' to another user?', MtInformation,
      [Mbyes, Mbno, Mbcancel], 0) <> Mryes Then
      Exit
    Else
    Begin
      if Not assigned(CloneUser) then
      begin
        CloneUser := TCloneUser.Create(nil);
        cloneuser.Caption := format(cloneuser.Caption,[cbuser.Text]);
        try
          cloneuser.cbCopyToUser.Items := cbUser.Items;
          usersmenus.First;
          for I := 0 to usersmenus.RecordCount-1 do
            begin
              cloneuser.LbMenuItems.Items.Add(usersmenus.FieldByName('Title').Text+'|'+usersmenus.FieldByName('ID').Text);
              usersmenus.Next;
            end;
          CloneUser.ShowModal;
        finally
          FreeAndNil(CloneUser);
        end;
      end;
    End;
  End;
End;

Procedure TSetupfrm.BtnDeleteMenuOptionClick(Sender: TObject);
Var
  UserNo: String;
  MenuId: String;
  X: Integer;
  Msgtext: String;
Begin
  Menuid := Dbgrid3.DataSource.DataSet.FieldByName('Title').Text;
  UserNo := Cbuser.Text; // Dbgrid3.DataSource.DataSet.FieldByName('LogonName').Text;

  If (Menuid = '') Or (UserNo = '') Then
    Exit;
  X := Pos('|', UserNo);
  UserNo := Copy(UserNo, 1, X - 1);
  Msgtext :=
    Format('You are about to delete the menu %s option for this user %s. Do you want to continue?',
    [Menuid, Userno]);
  If (MessageBox(0, Pchar(Msgtext), 'Delete Menu Option', MB_ICONQUESTION Or MB_YESNOCANCEL)
    In [IdYes]) Then
  Begin
    Q1.Close;
    Q1.SQL.Clear;
    Q1.SQL.Add('Delete From CITRIX_APPS.USE_MENU ');
    Q1.SQL.Add('WHERE userid = (SELECT user_no FROM citrix_apps.app_users WHERE logonname = ' +
      Quotedstr(UserNo) + ')');
    Q1.SQL.Add('AND menu_no = (SELECT id FROM citrix_apps.users_menus WHERE title = ' +
      QuotedStr(MenuId) + ')');
    // Q1.SQL.Add('Where UserId = ' + Quotedstr(UserNo) + 'and Menu_No = ' + QuotedStr(MenuId));
    Try
      // Q1.Debug := True;
      Q1.Execute;
      ShowMessage('Deleted this menu option');
      Session.Commit;
      Usersmenus.Refresh;
    Except
      On E: Exception Do
        ShowMessage('Failed to delete this record ' + E.Message);
    End;

  End;
End;

Procedure TSetupfrm.BtnUpdateClick(Sender: TObject);
Var
  UserNo: String;
  MenuId: String;
  X: Integer;
Begin
  X := Pos('|', Cbuser.Text);
  UserNo := Copy(Cbuser.Text, X + 1, 5);
  X := Pos('|', CbMenu.Text);
  MenuId := Copy(CbMenu.Text, X + 1, 5);
  If (UserNo = '') Or (MenuId = '') Then
    Exit;

  Q1.Close;
  Q1.SQL.Clear;
  Q1.SQL.Add('Insert into CITRIX_APPS.USE_MENU (USERID,MENU_NO) VALUES (');
  Q1.SQL.Add(UserNo + ',' + MenuId);
  Q1.SQL.Add(')');
  // q1.SQL.SaveToFile('c:\userdata\menuoption.txt');
  Try
    Try
      Q1.Execute;
      ShowMessage('Your update has been successfull');
    Except
      On E: Exception Do
        ShowMessage('Item not added due to Exception ' + E.Message);
    End;
  Finally
    Session.Commit;
    Usersmenus.Refresh;
  End;

End;

Procedure TSetupfrm.DBGrid1DrawColumnCell(Sender: TObject; Const Rect: TRect; DataCol: Integer;
  Column: TColumn; State: TGridDrawState);
Var
  Bitmap: TBitmap;
  FixRect: TRect;
  BmpWidth: Integer;
  ImgIndex: Integer;
Begin
  Fixrect := Rect;
  If Column.Field = MenudataICON_NUMBER Then
  Begin
    ImgIndex := MenudataICON_NUMBER.AsInteger;
    Bitmap := TBitMap.Create;
    Try
      // grab the image from the ImageList
      LargeImages.GetBitmap(ImgIndex, Bitmap);
      // Fix the bitmap dimensions
      BmpWidth := (Rect.Bottom - Rect.Top);
      FixRect.Right := Rect.Left + BmpWidth;
      // draw the bitmap
      DBGrid1.Canvas.StretchDraw(FixRect, Bitmap);
    Finally
      BitMap.FreeImage;
    End;
    // reset the output rectangle,
    // add space for the graphics
    FixRect := Rect;
    FixRect.Left := FixRect.Left + BmpWidth;
  End;
  DBGrid1.DefaultDrawColumnCell(FixRect, DataCol, Column, State);

End;

Procedure TSetupfrm.DBGrid1TitleClick(Column: TColumn);
Var
  Sortfield: String;

Begin
  Sortfield := Column.Field.FieldName;
  MenuData.Close;
  MenuData.SQL.Clear;
  MenuData.SQL.Add('select s.* , s.rowid from citrix_apps.users_menus s order by ' + SortField);
  MenuData.Open;
  // //DBGrid1.Refresh;
End;

Procedure TSetupfrm.DBGrid2TitleClick(Column: TColumn);
Var
  Sortfield: String;
Begin
  // we are closing the dataset to requery, need to get the column name first to use as the sort order by
  Sortfield := Column.Field.FieldName;
  Users.Close;
  Try
    Users.SQL.Clear;
    Users.SQL.Add('select u.*, u.rowid from citrix_apps.app_users u where u.user_no < 201 order by '
      + Sortfield);
    Users.Open;
  Except
    On E: Exception Do
      Showmessage('Excpetion here : ' + E.Message);
  End;

End;

Procedure TSetupfrm.DBGrid3CellClick(Column: TColumn);
VAR
  S, M: String;
Begin
  M := Column.Field.Text;
  S := Dbgrid3.Fields[1].AsString;
  Q1.Close;
  Q1.SQL.Clear;
  Q1.SQL.Add('Select User_No from CITRIX_APPS.App_Users where LOGONNAME = ' + Quotedstr(S));
  Q1.Execute;
  If Q1.RowsProcessed = 1 Then
  Begin
    CbUser.Text := S + '|' + IntToStr(Q1.Field('User_No'));
    // Cbmenu.Text := M;
  End;
End;

Procedure TSetupfrm.EdFindKeyPress(Sender: TObject; Var Key: Char);
Begin
  If Key = #13 Then
  Begin
    UsersMenus.Close;

    UsersMenus.SQL.Clear;
    UsersMenus.SQL.Add
      ('SELECT USERS_MENUS.TITLE, APP_USERS.LOGONNAME, Users_Menus.ID FROM CITRIX_APPS.APP_USERS APP_USERS,');
    UsersMenus.SQL.Add('CITRIX_APPS.USERS_MENUS USERS_MENUS, CITRIX_APPS.USE_MENU USE_MENU');
    UsersMenus.SQL.Add
      (' WHERE (APP_USERS.USER_NO = USE_MENU.USERID) AND (USE_MENU.MENU_NO = USERS_MENUS.ID)');
    UsersMenus.SQL.Add('and Logonname lIKE ' + Quotedstr(Edfind.Text) + '||+''%''');
    UsersMenus.SQL.Add('Order by Users_Menus.Title');
    UsersMenus.Active := True;
    DBGrid3.Refresh;
    Key := #0;
  End;

End;

Procedure TSetupfrm.FormCreate(Sender: TObject);
Begin
  Menudata.Active := True;
  Users.Active := True;
  Usersmenus.Active := True;
End;

Procedure TSetupfrm.FormShow(Sender: TObject);
Begin
  UpdateGUIControls;
End;

Procedure TSetupfrm.OracleNavigator1Click(Sender: TObject; Button: TOracleNavigateBtn);
Begin
  If Button = NbRefresh Then
  Begin
    MenuData.Refresh;
    UpdateGUIControls;
  End;
End;

Procedure TSetupfrm.UpdateGUIControls;
Var
  I: Integer;
Begin
  For I := 0 To Users.RecordCount - 1 Do
  Begin
    CbUser.Items.Add(Users.FieldByName('LOGONNAME').Text + ' | ' +
      Users.FieldByName('USER_NO').Text);
    Users.Next;
  End;
  For I := 0 To MenuData.RecordCount - 1 Do
  Begin
    Cbmenu.Items.Add(MenuData.FieldByName('TITLE').Text + ' | ' + MenuData.FieldByName('ID').Text);
    MenuData.Next;
  End;
  Cbmenu.Refresh;
  Cbuser.Refresh;
  MenuData.First;
  Users.First;
  UsersMenus.First;
End;

Procedure TSetupfrm.SpeedButton1Click(Sender: TObject);
Begin
  Showmessage('Select a user from the combo box labelled User Names' + #13#10 +
    'Select a menu option from the combo box labelled Menu options ' + #13#10 +
    'Click the button Update Menu Options ' + #13#10 + 'Repeat as required');
End;

Procedure TSetupfrm.SpeedButton2Click(Sender: TObject);
Begin
  EdFind.Clear;
  UsersMenus.Close;
  UsersMenus.SQL.Clear;
  UsersMenus.SQL.Add
    ('SELECT USERS_MENUS.TITLE, APP_USERS.LOGONNAME, Users_Menus.ID FROM CITRIX_APPS.APP_USERS APP_USERS, CITRIX_APPS.USERS_MENUS USERS_MENUS,');
  UsersMenus.SQL.Add('CITRIX_APPS.USE_MENU USE_MENU  WHERE (APP_USERS.USER_NO = USE_MENU.USERID)');
  UsersMenus.SQL.Add
    ('AND (USE_MENU.MENU_NO = USERS_MENUS.ID) ORDER BY APP_USERS.LOGONNAME ASC, USERS_MENUS.TITLE ASC');
  UsersMenus.Active := True;
  UsersMenus.Refresh;
  cbUser.Clear;
  cbMenu.Clear;
  UpdateGUIControls;
End;

End.
