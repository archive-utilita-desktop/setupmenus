object CloneUser: TCloneUser
  Left = 0
  Top = 0
  ActiveControl = cbCopyToUser
  Caption = 'Clone from %s '
  ClientHeight = 201
  ClientWidth = 407
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 407
    Height = 13
    Align = alTop
    Caption = 
      'Select the user that you want to copy the menu options to. Then ' +
      'press Clone'
    ExplicitWidth = 371
  end
  object Label2: TLabel
    Left = 8
    Top = 24
    Width = 54
    Height = 13
    Caption = 'Select User'
  end
  object btnClone: TBitBtn
    Left = 323
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Clo&ne'
    Kind = bkRetry
    NumGlyphs = 2
    TabOrder = 0
    OnClick = btnCloneClick
  end
  object btnExit: TBitBtn
    Left = 323
    Top = 152
    Width = 75
    Height = 25
    Kind = bkClose
    NumGlyphs = 2
    TabOrder = 1
  end
  object cbCopyToUser: TComboBoxEx
    Left = 8
    Top = 40
    Width = 273
    Height = 22
    ItemsEx = <>
    TabOrder = 2
  end
  object lbMenuItems: TListBox
    Left = 8
    Top = 72
    Width = 273
    Height = 105
    ItemHeight = 13
    TabOrder = 3
  end
end
