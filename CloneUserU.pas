Unit CloneUserU;

Interface

Uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.Buttons;

Type
  TCloneUser = Class(TForm)
    BtnClone: TBitBtn;
    BtnExit: TBitBtn;
    CbCopyToUser: TComboBoxEx;
    LbMenuItems: TListBox;
    Label1: TLabel;
    Label2: TLabel;
    Procedure BtnCloneClick(Sender: TObject);
  Private
    { Private declarations }
  Public
    { Public declarations }
  End;

Var
  CloneUser: TCloneUser;

Implementation

{$R *.dfm}

Uses SetupU;

Procedure TCloneUser.BtnCloneClick(Sender: TObject);
Var
  X: INteger;
  UserNo: String;
  MenuNo: String;
  Y: Integer;
Begin
  If LbMenuItems.Count > 0 Then
  Begin
    X := Pos('|', CBCopyToUser.Text);
    UserNo := Trim(Copy(CbCopyTouser.Text, X + 1, 5));
    Try
      For Y := 0 To LbMenuItems.Items.Count - 1 Do
      Begin
        X := Pos('|', Lbmenuitems.Items.Strings[Y]);
        MenuNo := Copy(LbMenuItems.Items.Strings[Y], X + 1, 5);
        If (UserNo = '') Or (MenuNo = '') Then
          Exit;
        Setupfrm.Q1.Close;
        Setupfrm.Q1.SQL.Clear;
        Setupfrm.Q1.SQL.Add('Insert into CITRIX_APPS.USE_MENU (USERID,MENU_NO) VALUES (');
        Setupfrm.Q1.SQL.Add(UserNo + ',' + MenuNo);
        Setupfrm.Q1.SQL.Add(')');
        Try
          Setupfrm.Q1.Execute;
          ShowMessage('Your update has been successfull');
        Except
          On E: Exception Do
          begin
            if pos('ORA-00001',e.Message) > 0 then
            e.Message := 'Duplicate Data';
            ShowMessage('Item not added due to ' + E.Message);
          end;
        End;
      End;
    Finally
      Setupfrm.Session.Commit;
    End;
  End;

End;

End.
